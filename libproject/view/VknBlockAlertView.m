//
//  AloeBlockAlertView.m
//  libproject
//
//  Created by kawase yu on 2013/09/10.
//  Copyright (c) 2013年 aloe-project All rights reserved.
//

#import "VknBlockAlertView.h"

@implementation VknBlockAlertView


@synthesize onClicked;
@synthesize onCancel;
@synthesize onDidPresent;
@synthesize onWillPresent;
@synthesize onWillDismiss;
@synthesize onDidDismiss;
@synthesize onShouldEnableOtherButton;

-(id)init{
    self = [super init];
    if(self){
        self.delegate = self;
    }
    
    return self;
}

// UIAlertViewDelegate ==================================

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(onClicked){
        onClicked(self, buttonIndex);
    }
}

// Called when we cancel a view (eg. the user clicks the Home button). This is not called when the user clicks the cancel button.
// If not defined in the delegate, we simulate a click in the cancel button
- (void)alertViewCancel:(UIAlertView *)alertView{
    if(onCancel){
        onCancel(self);
    }
}

- (void)willPresentAlertView:(UIAlertView *)alertView{
    if(onWillPresent){
        onWillPresent(self);
    }
}

- (void)didPresentAlertView:(UIAlertView *)alertView{
    if(onDidPresent){
        onDidPresent(self);
    }
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(onWillDismiss){
        onWillDismiss(self, buttonIndex);
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(onDidDismiss){
        onDidDismiss(self, buttonIndex);
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView{
    
    if(onShouldEnableOtherButton){
        return onShouldEnableOtherButton(self);
    }
    
    return true;
}

@end
