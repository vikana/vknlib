//
//  AloeBlockAlertView.h
//  libproject
//
//  Created by kawase yu on 2013/09/10.
//  Copyright (c) 2013年 aloe-project All rights reserved.
//

#import <UIKit/UIKit.h>

@class VknBlockAlertView;

typedef void (^VknAlertViewClicked_t)(VknBlockAlertView* alertView, int index);
typedef void (^VknAlertViewCancel_t)(VknBlockAlertView* alertView);
typedef void (^VknAlertViewDidPresentAlert_t)(VknBlockAlertView* alertView);
typedef void (^VknAlertViewWillPresentAlert_t)(VknBlockAlertView* alertView);
typedef void (^VknAlertViewWillDismissWithButtonIndex_t)(VknBlockAlertView* alertView, int index);
typedef void (^VknAlertViewDidDismissWithButtonIndex_t)(VknBlockAlertView* alertView, int index);
typedef BOOL (^VknAlertViewShouldEnableFirstOtherButton_t)(VknBlockAlertView* alertView);


@interface VknBlockAlertView : UIAlertView{
    VknAlertViewClicked_t onClicked;
    VknAlertViewCancel_t onCancel;
    VknAlertViewDidPresentAlert_t onDidPresent;
    VknAlertViewWillPresentAlert_t onWillPresent;
    VknAlertViewWillDismissWithButtonIndex_t onWillDismiss;
    VknAlertViewDidDismissWithButtonIndex_t onDidDismiss;
    VknAlertViewShouldEnableFirstOtherButton_t onShouldEnableOtherButton;
}

@property (strong) VknAlertViewClicked_t onClicked;
@property (strong) VknAlertViewCancel_t onCancel;
@property (strong) VknAlertViewDidPresentAlert_t onDidPresent;
@property (strong) VknAlertViewWillPresentAlert_t onWillPresent;
@property (strong) VknAlertViewWillDismissWithButtonIndex_t onWillDismiss;
@property (strong) VknAlertViewDidDismissWithButtonIndex_t onDidDismiss;
@property (strong) VknAlertViewShouldEnableFirstOtherButton_t onShouldEnableOtherButton;


@end
