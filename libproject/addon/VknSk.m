//
//  VKNSk.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknSk.h"

@implementation VknSk

@synthesize onPurchaseSuccess;
@synthesize onFail;
@synthesize onComplete;
@synthesize onCancel;
@synthesize onRestoreSuccess;
@synthesize onRestoreComplete;

// ▼singleton ============================
// http://programming-ios.com/objective-c-singleton/ より

+ (VknSk*)getInstance {
    static VknSk* sharedSingleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[VknSk alloc] initInstance];
    });
    return sharedSingleton;
}

- (id)initInstance {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)init {
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}

// ▲ singleton ===============================

-(void) initialize{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    productList = [[NSMutableDictionary alloc] init];
    state = VKNSK_WAIT;
    currentRequestType = VKNSK_REQUEST_NONE;
}

// ▼ SKProductsRequestDelegate =================

// TODO 複数対応
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    NSArray* products = response.products;
    
    state = VKNSK_WAIT;
    
    // productIdentifierが見つからない
    if([products count] == 0){
        currentRequestType = VKNSK_REQUEST_NONE;
        NSError* error = [NSError errorWithDomain:@"productIdentifierが無いよerrorDomain"
                                             code:1
                                         userInfo:@{@"debugDescription":@"たぶん無効なproductIdentifier"}];
        if(onFail)
            onFail(error);
        if(onComplete)
            onComplete();
        return;
    }
    
    for(SKProduct* product in products){
        if([productList objectForKey:product.productIdentifier] != nil)
            continue;
        [productList setObject:product forKey:product.productIdentifier];
    }
    
    SKProduct* product = [products objectAtIndex:0];
    if(currentRequestType == VKNSK_REQUEST_PURCHASE){
        [self purchaseImple:product];
    }
}

// ▼ SKPaymentTransactionObserver ==============

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){
    for(SKPaymentTransaction* transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"SKPaymentTransactionStatePurchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                NSLog(@"SKPaymentTransactionStatePurchased");
                if(onPurchaseSuccess){
                    NSString* receipt = [transaction.transactionReceipt stringEncodedWithBase64];
                    onPurchaseSuccess(transaction.payment.productIdentifier, receipt);
                }
                [self finishRequest:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"SKPaymentTransactionStateFailed");
                if(onFail)
                    onFail(transaction.error);
                [self finishRequest:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                if(onRestoreSuccess){
                    NSString* receipt = [transaction.transactionReceipt stringEncodedWithBase64];
                    onRestoreSuccess(transaction.payment.productIdentifier, receipt);
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){
    NSLog(@"removedTransactions");
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){
    NSLog(@"restoreCompletedTransactionsFailedWithError");
    if(onFail)
        onFail(error);
    if(onComplete)
        onComplete();
    state = VKNSK_WAIT;
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0){
    NSLog(@"paymentQueueRestoreCompletedTransactionsFinished");
    if(onRestoreComplete)
        onRestoreComplete();
    if(onComplete)
        onComplete();
    state = VKNSK_WAIT;
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0){
    NSLog(@"updatedDownloads");
}

// ▼ private ===============================

-(void) finishRequest:(SKPaymentTransaction*) transaction{
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    if(onComplete)
        onComplete();
    state = VKNSK_WAIT;
    currentRequestType = VKNSK_REQUEST_NONE;
}

-(void) purchaseImple:(SKProduct*)product{
    state = VKNSK_PURCHASE_REQUEST;
    SKPayment* payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(void) restoreImple{
    state = VKNSK_RESTORE_REQUEST;
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(void) productRequest:(NSString*)productIdentifier{
    SKProductsRequest* productRequest = [[SKProductsRequest alloc]
                                         initWithProductIdentifiers:[[NSSet alloc] initWithObjects:productIdentifier, nil]];
    productRequest.delegate = self;
    [productRequest start];
}

-(void) cantSk{
    UIAlertView* alertView = [[UIAlertView alloc] init];
    alertView.title = @"購入ができません";
    alertView.message = @"端末の機能制限でApp内での購入が不可になっています\n\n設定 > 一般 > 機能制限で\n[App内での購入]を\nONにしてください";
    [alertView addButtonWithTitle:@"OK"];
    [alertView show];
    
    [self cancel];
}

-(void) cancel{
    if(onCancel)
        onCancel();
    if(onComplete)
        onComplete();
}

// ▼ public =================================

-(void) purchase:(NSString*)productIdentifier{
    if(![self checkSk]){
        [self cantSk];
        return;
    }
    
    if([self isConnectiong]){
        NSLog(@"cancel on purchasing.....");
        [self cancel];
        
        return;
    }
    
    // プロダクト情報が既にあればそれを使う
    SKProduct* product = [productList objectForKey:productIdentifier];
    if(product != nil) {
        [self purchaseImple:product];
        return;
    }
    
    // なければとりにいく
    state = VKNSK_PRODUCT_REQUEST;
    currentRequestType = VKNSK_REQUEST_PURCHASE;
    [self productRequest:productIdentifier];
}

-(void) restore{
    if(![self checkSk]){
        [self cantSk];
        return;
    }
    
    if([self isConnectiong]){
        NSLog(@"cancel on purchasing.....");
        [self cancel];
        return;
    }
    
    state = VKNSK_PRODUCT_REQUEST;
    currentRequestType = VKNSK_REQUEST_RESTORE;
    [self restoreImple];
}

-(BOOL) checkSk{
    return [SKPaymentQueue canMakePayments];
}

-(BOOL) isConnectiong{
    return (state != VKNSK_WAIT);
}

@end