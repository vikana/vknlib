//
//  VKNSk.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

/**
 @example 購入処理
 
 SolSk* solsk = [SolSk getInstance];
 solsk.onFail = ^(NSError* error){
 DLog(@"%@", error.debugDescription);
 };
 solsk.onPurchaseSuccess = ^(NSString* productIdentifier, NSString* receipt){
 DLog(@"%@ 購入完了", productIdentifier);
 };
 [solsk puchase:@"com.subakolab.beauty.money1"];
 
 */

/**
 @example リストア
 
 SolSk* solsk = [SolSk getInstance];
 solsk.onFail = ^(NSError* error){
 DLog(@"%@", error.debugDescription);
 };
 // リストア対象のアドオンの数だけ呼ばれる
 solsk.onRestoreSuccess = ^(NSString* productIdentifier, NSString* receipt){
 DLog(@"リストアアドオン：%@", productIdentifier);
 };
 // リストアが全部終わったら呼ばれる
 solsk.onRestoreComplete = ^(){
 DLog(@"リストア完了！");
 };
 */

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "NSData+Base64.h"

typedef enum VKNSK_STATE{
    VKNSK_PRODUCT_REQUEST,
    VKNSK_PURCHASE_REQUEST,
    VKNSK_RESTORE_REQUEST,
    VKNSK_WAIT
} VKNSKState;

typedef enum VKNSK_REQUEST_TYPE{
    VKNSK_REQUEST_PRODUCT,
    VKNSK_REQUEST_PURCHASE,
    VKNSK_REQUEST_RESTORE,
    VKNSK_REQUEST_NONE
} VKNSKRequestType;

typedef void (^VKNSKOnPurchaseSuccess_t)(NSString* productIdentifier, NSString* receipt);
typedef void (^VKNSKOnFail_t)(NSError* error);
typedef void (^VKNSKOnCancel_t)();
typedef void (^VKNSKOnRestoreSuccess_t)(NSString* productIdentifier, NSString* receipt);
typedef void (^VKNSKOnRestoreComplete_t)();
typedef void (^VKNSKOnComplete_t)();

@interface VknSk : NSObject<
SKProductsRequestDelegate
, SKPaymentTransactionObserver
>{
    // callbacks
    VKNSKOnPurchaseSuccess_t onPurchaseSuccess;
    VKNSKOnFail_t onFail;
    VKNSKOnCancel_t onCancel;
    VKNSKOnRestoreSuccess_t onRestoreSuccess;
    VKNSKOnComplete_t onComplete;
    VKNSKOnRestoreComplete_t onRestoreComplete;
    
    // params
    VKNSKState state;
    VKNSKRequestType currentRequestType;
    NSMutableDictionary* productList;
}

@property (strong) VKNSKOnPurchaseSuccess_t onPurchaseSuccess;
@property (strong) VKNSKOnFail_t onFail;
@property (strong) VKNSKOnCancel_t onCancel;
@property (strong) VKNSKOnRestoreSuccess_t onRestoreSuccess;
@property (strong) VKNSKOnRestoreComplete_t onRestoreComplete;
@property (strong) VKNSKOnComplete_t onComplete;

// singleton
+ (VknSk*)getInstance;

// 処理
-(void) purchase:(NSString*)productIdentifier;
-(void) restore;
// 確認
-(BOOL) checkSk;
-(BOOL) isConnectiong;

@end
