//
//  VknTween.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const VknTweenEaseNone;
extern NSString* const VknTweenEaseInQuad;
extern NSString* const VknTweenEaseOutQuad;
extern NSString* const VknTweenEaseInOutQuad;
extern NSString* const VknTweenEaseInCubic;
extern NSString* const VknTweenEaseOutCubic;
extern NSString* const VknTweenEaseInOutCubic;
extern NSString* const VknTweenEaseInQuart;
extern NSString* const VknTweenEaseInOutQuart;
extern NSString* const VknTweenEaseInQuint;
extern NSString* const VknTweenEaseOutQuint;
extern NSString* const VknTweenEaseInOutQuint;
extern NSString* const VknTweenEaseInSine;
extern NSString* const VknTweenEaseOutSine;
extern NSString* const VknTweenEaseInOutSine;
extern NSString* const VknTweenEaseInExpo;
extern NSString* const VknTweenEaseOutExpo;
extern NSString* const VknTweenEaseInOutExpo;
extern NSString* const VknTweenEaseInCirc;
extern NSString* const VknTweenEaseOutCirc;
extern NSString* const VknTweenEaseInOutCirc;
extern NSString* const VknTweenEaseInElastic;
extern NSString* const VknTweenEaseOutElastic;
extern NSString* const VknTweenEaseInOutElastic;
extern NSString* const VknTweenEaseInBack;
extern NSString* const VknTweenEaseOutBack;
extern NSString* const VknTweenEaseInOutBack;
extern NSString* const VknTweenEaseInBounce;
extern NSString* const VknTweenEaseOutBounce;
extern NSString* const VknTweenEaseInOutBounce;

typedef void (^VknTweenUpdateCallback)(double value);
typedef void (^VknTweenCompleteCallback)();

@interface VknTween : NSObject

+(void) tween:(float) duration easing:(NSString*)easing update:(VknTweenUpdateCallback)callback;
+(void) tween:(float) duration easing:(NSString*)easing update:(VknTweenUpdateCallback)callback complete:(VknTweenCompleteCallback)completeCallback;


@end
