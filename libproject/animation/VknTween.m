//
//  VknTween.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknTween.h"
#import "Tween.h"
#import "TweenObject.h"

#pragma mark -- const --

 NSString* const VknTweenEaseNone = @"easeNone";
 NSString* const VknTweenEaseInQuad = @"easeInQuad";
 NSString* const VknTweenEaseOutQuad = @"easeOutQuad";
 NSString* const VknTweenEaseInOutQuad = @"easeInOutQuad";
 NSString* const VknTweenEaseInCubic = @"easeInCubic";
 NSString* const VknTweenEaseOutCubic = @"easeOutCubic";
 NSString* const VknTweenEaseInOutCubic = @"easeInOutCubic";
 NSString* const VknTweenEaseInQuart = @"easeInQuart";
 NSString* const VknTweenEaseInOutQuart = @"easeOutQuart";
 NSString* const VknTweenEaseInQuint = @"easeInOutQuart";
 NSString* const VknTweenEaseOutQuint = @"easeInQuint";
 NSString* const VknTweenEaseInOutQuint = @"easeOutQuint";
 NSString* const VknTweenEaseInSine = @"easeInOutQuint";
 NSString* const VknTweenEaseOutSine = @"easeInSine";
 NSString* const VknTweenEaseInOutSine = @"easeOutSine";
 NSString* const VknTweenEaseInExpo = @"easeInExpo";
 NSString* const VknTweenEaseOutExpo = @"easeOutExpo";
 NSString* const VknTweenEaseInOutExpo = @"easeInOutExpo";
 NSString* const VknTweenEaseInCirc = @"easeInCirc";
 NSString* const VknTweenEaseOutCirc = @"easeOutCirc";
 NSString* const VknTweenEaseInOutCirc = @"easeInOutCirc";
 NSString* const VknTweenEaseInElastic = @"easeInElastic";
 NSString* const VknTweenEaseOutElastic = @"easeOutElastic";
 NSString* const VknTweenEaseInOutElastic = @"easeInOutElastic";
 NSString* const VknTweenEaseInBack = @"easeInBack";
 NSString* const VknTweenEaseOutBack = @"easeOutBack";
 NSString* const VknTweenEaseInOutBack = @"easeInOutBack";
 NSString* const VknTweenEaseInBouncev = @"easeInBounce";
 NSString* const VknTweenEaseOutBounce = @"easeOutBounce";
 NSString* const VknTweenEaseInOutBounce = @"easeInOutBounce";

#pragma mark -- 内部クラス --

@class VknTweenObject;

@protocol VknTweenObjectDelegate <NSObject>

+(void) onEndTween:(VknTweenObject*)obj;

@end

@interface VknTweenObject : NSObject{
    float duration;
    NSString* easing;
    VknTweenUpdateCallback callback;
    VknTweenCompleteCallback completeCallback;
    id<VknTweenObjectDelegate> delegate;
}

-(id) initWithDelegate:(id<VknTweenObjectDelegate>)delegate_ duration:(float)duration_ easing:(NSString*)easing_ callback:(VknTweenUpdateCallback)callback_;
-(id) initWithDelegate:(id<VknTweenObjectDelegate>)delegate_ duration:(float)duration_ easing:(NSString*)easing_ callback:(VknTweenUpdateCallback)callback_ completeCallback:(VknTweenCompleteCallback)completeCallback_;
-(void) execute;

@end

@implementation VknTweenObject

-(id) initWithDelegate:(NSObject<VknTweenObjectDelegate>*)delegate_ duration:(float)duration_ easing:(NSString *)easing_ callback:(VknTweenUpdateCallback)callback_{
    self = [super init];
    if(self){
        delegate = delegate_;
        duration = duration_;
        easing = easing_;
        callback = callback_;
    }
    
    return self;
}

-(id) initWithDelegate:(id<VknTweenObjectDelegate>)delegate_ duration:(float)duration_ easing:(NSString *)easing_ callback:(VknTweenUpdateCallback)callback_ completeCallback:(VknTweenCompleteCallback)completeCallback_{
    self = [self initWithDelegate:delegate_ duration:duration_ easing:easing_ callback:callback_];
    if(self){
        completeCallback = completeCallback_;
    }
    
    return self;
}

-(void) execute{
    [Tween addTween:self tweenId:0
         startValue:0.0
           endValue:1.0
               time:duration
              delay:0
             easing:easing
           startSEL:@selector(onStart:)
          updateSEL:@selector(onUpdate:)
             endSEL:@selector(onEnd:)];
}

-(void) onStart:(TweenObject*)tweenObject{
    double value = tweenObject.currentValue;
    callback(value);
}

-(void) onUpdate:(TweenObject*)tweenObject{
    double value = tweenObject.currentValue;
    callback(value);
}

-(void) onEnd:(TweenObject*)tweenObject{
    double value = tweenObject.currentValue;
    callback(value);
    if(completeCallback)
        completeCallback();
    [delegate onEndTween:self];
}

@end

#pragma mark -- メイン SolTween --

@interface VknTween()<VknTweenObjectDelegate>

@end

@implementation VknTween

static NSMutableArray* objList;
+(NSMutableArray*)objList{
    if(objList != nil) return objList;
    
    objList = [[NSMutableArray alloc] init];
    return objList;
}

static NSMutableDictionary* completeCallbackList;
+(NSMutableDictionary*)completeCallbackList{
    if( completeCallbackList != nil) return completeCallbackList;
    
    completeCallbackList = [[NSMutableDictionary alloc] init];
    return completeCallbackList;
}

+(void) tween:(float) duration easing:(NSString*)easing update:(VknTweenUpdateCallback)callback{
    VknTweenObject* obj = [[VknTweenObject alloc] initWithDelegate:(id<VknTweenObjectDelegate>)self duration:duration easing:easing callback:callback];
    [[self objList] addObject:obj];
    [obj execute];
}

+(void) tween:(float) duration easing:(NSString*)easing update:(VknTweenUpdateCallback)callback complete:(VknTweenCompleteCallback)completeCallback{
    VknTweenObject* obj = [[VknTweenObject alloc] initWithDelegate:(id<VknTweenObjectDelegate>)self
                                                          duration:duration
                                                            easing:easing
                                                          callback:callback
                                                  completeCallback:completeCallback];
    [[self objList] addObject:obj];
    [obj execute];
}

+(void) onEndTween:(VknTweenObject *)obj{
    [objList removeObject:obj];
}

@end
