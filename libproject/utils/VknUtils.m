//
//  VknUtils.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknUtils.h"
#include <sys/xattr.h>

@implementation VknUtils

+ (NSUInteger)randIntRange:(NSRange)range{
	return range.location + (NSUInteger)([self randInt] * (range.length + 1.0) / (RAND_MAX + 1.0));
}

+(int) randInt:(int)max{
    return arc4random_uniform(max);
}

+ (NSUInteger)randInt
{
	[self staticInit];
	return rand();
}

+ (void)staticInit
{
	static BOOL initFlag = FALSE;
	if (!initFlag) {
		srand(time(NULL));
		initFlag = TRUE;
	}
}

+ (NSString*)appStoreUrl:(NSString*) appId{
    return [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@", appId];
}

+(BOOL) isNull:(id)obj{
    return (obj == nil || [obj isEqual:[NSNull null]]);
}

+(BOOL) isNotNull:(id)obj{
    return ![self isNull:obj];
}

+ (UInt64)getEpochSeconds
{
    return (UInt64)floor(CFAbsoluteTimeGetCurrent() + kCFAbsoluteTimeIntervalSince1970);
}

+ (UInt64)getEpochMilliSeconds
{
    return (UInt64)floor((CFAbsoluteTimeGetCurrent() + kCFAbsoluteTimeIntervalSince1970) * 1000.0);
}

+(void) openBrowser:(NSString *)url{
    NSURL *nsurl = [NSURL URLWithString:url];
    [[UIApplication sharedApplication] openURL:nsurl];
}

+(void) wait:(float)millisec callback:(VknUtilsCallback_t)callback{
    [VknThreadUtil threadBlock:^{
        [NSThread sleepForTimeInterval:millisec];
    } callback:^{
        callback();
    }];
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    const char* filePath = [[URL path] fileSystemRepresentation];
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}

@end


@implementation NSArray (VknArray)

- (NSArray *)shuffled
{
    NSMutableArray *tmpArray = [NSMutableArray arrayWithCapacity:[self count]];
    
    for(id object in self)
    {
        NSUInteger randomNum = arc4random() % ([tmpArray count] + 1);
        [tmpArray insertObject:object atIndex:randomNum];
    }
    return tmpArray;
}
@end