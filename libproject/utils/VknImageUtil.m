//
//  VknImageUtil.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknImageUtil.h"

@implementation VknImageUtil

+ (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)width{
    float rate = image.size.height / image.size.width;
    float height = rate * width;
    return [self resizeImage:image width:width height:height];
}

+ (UIImage *)resizeImage:(UIImage *)image height:(CGFloat)height{
    float rate = image.size.width / image.size.height;
    float width = rate * height;
    
    return [self resizeImage:image width:width height:height];
}

+ (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)width height:(CGFloat)height
{
	if (UIGraphicsBeginImageContextWithOptions != NULL) {
		UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), NO, [[UIScreen mainScreen] scale]);
	} else {
		UIGraphicsBeginImageContext(CGSizeMake(width, height));
	}
    
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetInterpolationQuality(context, kCGInterpolationHigh); // 高品質リサイズ
    
	[image drawInRect:CGRectMake(0.0, 0.0, width, height)];
    
	UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    
    
	return resizedImage;
}

// http://qiita.com/items/3ad3aa92024b4f7401cd
+ (UIImage*)clipImage:(UIImage *)image rect:(CGRect)rect {
    
    // 画像のクリッピング iPad3 Retina対応
    float scale = [[UIScreen mainScreen] scale];
    CGRect scaledRect = CGRectMake(rect.origin.x * scale,
                                   rect.origin.y * scale,
                                   rect.size.width * scale,
                                   rect.size.height * scale);
    
    CGImageRef clip = CGImageCreateWithImageInRect(image.CGImage,scaledRect);
    UIImage *clipedImage = [UIImage imageWithCGImage:clip
                                               scale:scale
                                         orientation:UIImageOrientationUp];
    CGImageRelease(clip);
    
    return clipedImage;
}

+ (UIImage *)imageFromSampleBufferRef:(CMSampleBufferRef)sampleBuffer
{
    // イメージバッファの取得
    CVImageBufferRef    buffer;
    buffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // イメージバッファのロック
    CVPixelBufferLockBaseAddress(buffer, 0);
    // イメージバッファ情報の取得
    uint8_t*    base;
    size_t      width, height, bytesPerRow;
    base = CVPixelBufferGetBaseAddress(buffer);
    width = CVPixelBufferGetWidth(buffer);
    height = CVPixelBufferGetHeight(buffer);
    bytesPerRow = CVPixelBufferGetBytesPerRow(buffer);
        
    // ビットマップコンテキストの作成
    CGColorSpaceRef colorSpace;
    CGContextRef    cgContext;
    colorSpace = CGColorSpaceCreateDeviceRGB();
    cgContext = CGBitmapContextCreate(
                                      base, width, height, 8, bytesPerRow, colorSpace,
                                      kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);
    
    
    // 画像の作成
    CGImageRef  cgImage;
    UIImage*    image;
    cgImage = CGBitmapContextCreateImage(cgContext);
//    image = [UIImage imageWithCGImage:cgImage scale:1.0f
//                          orientation:UIImageOrientationRight];
    image = [UIImage imageWithCGImage:cgImage scale:1.0f
                          orientation:UIImageOrientationUp];
    CGImageRelease(cgImage);
    CGContextRelease(cgContext);
    
    // イメージバッファのアンロック
    CVPixelBufferUnlockBaseAddress(buffer, 0);
    return image;
}

+ (UIImage*) scaledImage:(UIImage*)image{
    CGFloat scale = [[ UIScreen mainScreen ] scale ];
    CGSize size = image.size;
    if( round( image.scale ) != round( scale )) {
        CGSize newSize = CGSizeMake( size.width / scale, size.height / scale );
        UIGraphicsBeginImageContextWithOptions( newSize, YES, scale );
        [ image drawInRect: CGRectMake( 0.0, 0.0, newSize.width, newSize.height )];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return image;
}

+(UIImage *)imageFromView:(UIView *)view{
    // 必要なUIImageサイズ分のコンテキスト確保
    UIGraphicsBeginImageContextWithOptions(view.frame.size, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();

    // 画像化する部分の位置を調整
    CGContextTranslateCTM(context, -view.frame.origin.x, -view.frame.origin.y);

    // 画像出力
    [view.layer renderInContext:context];

    // uiimage化
    UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();

    // コンテキスト破棄
    UIGraphicsEndImageContext();

    return renderedImage;
}

+(UIImage*) clipedImage:(UIImage *)image rect:(CGRect)rect{
    // 画像のクリッピング iPad3 Retina対応
    float scale = [[UIScreen mainScreen] scale];
    CGRect scaledRect = CGRectMake(rect.origin.x * scale,
                                   rect.origin.y * scale,
                                   rect.size.width * scale,
                                   rect.size.height * scale);
    
    CGImageRef clip = CGImageCreateWithImageInRect(image.CGImage,scaledRect);
    UIImage *clipedImage = [UIImage imageWithCGImage:clip
                                               scale:scale
                                         orientation:UIImageOrientationUp];
    
    return clipedImage;
}

+(UIImage*) turnedImage:(UIImage *)image{
    
    CGFloat srcWidth = CGImageGetWidth(image.CGImage);
    CGFloat srcHeight = CGImageGetHeight(image.CGImage);
    
    //オリジナルサイズ
    CGSize srcSize = CGSizeMake(srcWidth, srcHeight);
    CGContextRef ctx;
    
    UIGraphicsBeginImageContext(srcSize);
    UIGraphicsBeginImageContextWithOptions(srcSize, NO, 1);
    ctx = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(ctx, srcWidth, 0);
    CGContextScaleCTM(ctx, -1.0, 1.0);
    
    [image drawInRect:CGRectMake(0, 0, srcWidth, srcHeight)];
    
    UIImage *src2 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return src2;
}

@end
