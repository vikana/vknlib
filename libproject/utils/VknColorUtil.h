//
//  VknColorUtil.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

#define UIColorFromHex(hexValue) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:1.0]

/**---------------------------------------------
 @brief 色系ユーティリティ
 @note
 
 */
@interface VknColorUtil : NSObject

+(UIColor*) hexToUIColor:(NSString *)hex;

@end
