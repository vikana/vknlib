//
//  VknImageUtil.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface VknImageUtil : NSObject

+ (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)width;
+ (UIImage *)resizeImage:(UIImage *)image height:(CGFloat)height;
+ (UIImage*)clipImage:(UIImage *)image rect:(CGRect)rect;
+ (UIImage *)imageFromSampleBufferRef:(CMSampleBufferRef)sampleBuffer;
+ (UIImage*) scaledImage:(UIImage*)image;
+ (UIImage *)imageFromView:(UIView *)view;
+(UIImage*) clipedImage:(UIImage*)image rect:(CGRect)rect;
+(UIImage*) turnedImage:(UIImage*)image;

@end
