//
//  VknDataUtil.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

/**---------------------------------------------
 @brief データ系ユーティリティ
 @note
 
 */
@interface VknDataUtil : NSObject

+(NSDictionary*) dataToJson:(NSData*)obj;
+(NSArray*) dataToJsonArray:(NSData*)obj;
+(NSDictionary*) strToJson:(NSString*)str;
+(NSArray*) strToJsonArray:(NSString*)str;
+(NSString*) jsonToString:(NSDictionary*)json;
+(NSString*) dataToHexString:(NSData*)data;
+(NSData*) dicToData:(NSDictionary*)dic;
+(NSDictionary*) dataToDic:(NSData*)data;
+(NSString*) deviceTokenToStr:(NSData*)deviceToken;

/**---------------------------------------------
 @brief 画像データとして欠損がないか
 */
+(BOOL) isValidImage:(NSData *)data;
+(NSString*)dictionaryToQuerystring:(NSDictionary*)dic;
+(NSData*) strToData:(NSString*)str;
+(NSString*)dataToString:(NSData*)data;

@end
