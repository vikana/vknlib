//
//  VknFileUtil.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VknFileUtil : NSObject

+(NSString*)cachePath;
+(NSString*)documentPath;
+(NSString*) documentFilePath:(NSString*)fileName;
+(NSString*)createCachePath:(NSString*)path;
+(void)mkDir:(NSString*)dirPath;
+(void)mkDirInCache:(NSString*)path;
+(BOOL) isExists:(NSString*)path;

+(NSString*)cacheDir;
+(NSString*)cacheFilePath:(NSString*)fileName;

+(NSString*)tmpPath;
+(NSString*) tmpFilePath:(NSString*)fileName;

@end
