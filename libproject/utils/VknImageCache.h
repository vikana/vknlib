//
//  VknImageCache.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^onload_image_t)(UIImage* image, NSString* key, BOOL useCache);


/**---------------------------------------------
 @brief 画像キャッシュ
 (※必須)AppDelegateのmemoryWarningで 当クラスの clearを呼んでね
 */
@interface VknImageCache : NSObject

//+(void) create;
+(void) clear;
+(BOOL) hasCache:(NSString*)url;
+(UIImage*) getChache:(NSString*)url;
+(void) setCache:(UIImage*)image forKey:(NSString*)url;

+(void) loadImage:(NSString*)url
     defaultImage:(UIImage*)image
         callback:(onload_image_t)callback;


@end
