//
//  VknDateUtil.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknDateUtil.h"

@implementation VknDateUtil


+(NSDate*)stringToDate:(NSString*)pubDate format:(NSString*)format{
    if(dateFormatter == nil){
        dateFormatter = [[NSDateFormatter alloc] init];
    }
    
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"JST"]];
    [dateFormatter setDateFormat:format];
    NSDate* date = [dateFormatter dateFromString:pubDate];
    
    return date;
}

+(NSString*)dateToString:(NSDate*)date format:(NSString*)format{
    NSDateFormatter *toStringFormatter = [self getDateFormatter];
    toStringFormatter.dateFormat  = format;
    return [toStringFormatter stringFromDate:date];
}

// dateFormatter の生成がことのほかコスト
static NSDateFormatter* dateFormatter;
+(NSDateFormatter*) getDateFormatter{
    if(dateFormatter == nil){
        dateFormatter = [[NSDateFormatter alloc] init];
    }
    
    return dateFormatter;
}

@end
