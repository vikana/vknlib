//
//  VknDateUtil.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

/**---------------------------------------------
 @brief 日付系ユーティリティ
 */
@interface VknDateUtil : NSObject

+(NSString*)dateToString:(NSDate*)date format:(NSString*)format;
+(NSDate*)stringToDate:(NSString*)pubDate format:(NSString*)format;

@end
