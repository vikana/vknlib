//
//  VknThreadUtil.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

/**---------------------------------------------
 @brief スレッド(CGD)系ユーティリティ
 */
@interface VknThreadUtil : NSObject

typedef void (^VknThreadBlock_t)();

//+(void) create;
+(void) threadBlock:(VknThreadBlock_t)block;
+(void) mainBlock:(VknThreadBlock_t)block;


+(void) threadBlock:(VknThreadBlock_t)block
           callback:(VknThreadBlock_t)callback;



@end
