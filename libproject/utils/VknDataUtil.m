//
//  VknDataUtil.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknDataUtil.h"

@implementation VknDataUtil

+(NSDictionary*) dataToJson:(NSData*)obj{
    NSError* error = nil;
    
    NSDictionary* jsonDic = [NSJSONSerialization JSONObjectWithData:obj options:NSJSONReadingAllowFragments error:&error];
    if(error != nil){
        NSLog(@"NSJSONSerizationError:%@", error);
        return nil;
    }
    
    return jsonDic;
}

+(NSArray*) dataToJsonArray:(NSData*)obj{
    NSError* error = nil;
    
    NSArray* jsonArray = [NSJSONSerialization JSONObjectWithData:obj options:NSJSONReadingAllowFragments error:&error];
    if(error != nil){
        NSLog(@"NSJSONSerizationError:%@", error);
        return nil;
    }
    
    return jsonArray;
}

+(NSDictionary*) strToJson:(NSString *)str{
    NSData *jsonData = [str dataUsingEncoding:NSUnicodeStringEncoding];
    return [self dataToJson:jsonData];
}

+(NSArray*) strToJsonArray:(NSString *)str{
    NSData *jsonData = [str dataUsingEncoding:NSUnicodeStringEncoding];
    return [self dataToJsonArray:jsonData];
}

+(NSString*)jsonToString:(NSDictionary *)json{
    // TODO error
    NSError* error = nil;
    NSData* data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:&error];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+(NSString*)dataToHexString:(NSData *)data{
    NSString* str = [NSString stringWithFormat:@"%@",data];
    NSArray* stripCharactors = @[@"<", @">", @" "];
    for(NSString* s in stripCharactors){
        str = [str stringByReplacingOccurrencesOfString:s withString:@""];
    }
    return str;
}

+(NSData*)dicToData:(NSDictionary *)dic{
     return [NSKeyedArchiver archivedDataWithRootObject:dic];
}

+(NSDictionary*) dataToDic:(NSData*)data{
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

static NSDictionary* imageBinaryLastIdentifiers;
+(NSDictionary*) imageBinaryLastIdentifiers{
    if(imageBinaryLastIdentifiers != nil)
        return imageBinaryLastIdentifiers;
    
    // 画像バイナリの終端識別子
    // http://craftone.hatenablog.com/entry/2013/05/07/010539
    imageBinaryLastIdentifiers = @{
                                   @"jpg": @{
                                           @"identifier":@"ffd9"
                                           , @"len":@2
                                           }
                                   , @"png": @{
                                           @"identifier":@"0000000049454e44ae426082"
                                           , @"len":@12
                                           }
                                   , @"gif": @{
                                           @"identifier":@"TODO"
                                           , @"len":@1
                                           }
                                   };
    return imageBinaryLastIdentifiers;
}

+(BOOL) isValidImage:(NSData *)data{
    
    NSDictionary* imageBinaryLastIdentifiers = [self imageBinaryLastIdentifiers];
    
    int dataSize = [data length];
    
    for(NSString* ext in imageBinaryLastIdentifiers){
        NSDictionary* row = [imageBinaryLastIdentifiers valueForKey:ext];
        int checkSize = [[row objectForKey:@"len"] intValue];
        NSString* identifier = [row objectForKey:@"identifier"];
        NSRange checkRange = NSMakeRange(dataSize-checkSize, checkSize);
        NSData* checkData = [data subdataWithRange:checkRange];
        NSString* checkString = [VknDataUtil dataToHexString:checkData];
        
        if([checkString isEqualToString:identifier]){
            NSLog(@"ext:%@", ext);
            return YES;
        }
    }
    
    // 対象なし
    NSLog(@"undefined format");
    return NO;
}

+(NSString*)dictionaryToQuerystring:(NSDictionary*)dic{
    NSMutableString* ms = [[NSMutableString alloc] init];
    for(NSString* key in dic){
        id val = [dic objectForKey:key];
        [ms appendString:key];
        [ms appendString:@"="];
        if(![val isKindOfClass:[NSString class]]){
            val =  [val stringValue];
        }
        [ms appendString:val];
        [ms appendString:@"&"];
    }
    
    return [NSString stringWithString:ms];
}

+(NSData*) strToData:(NSString*)str{
    return [str dataUsingEncoding:NSUTF8StringEncoding];
}

+(NSString*)dataToString:(NSData*)data{
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+(NSString*) deviceTokenToStr:(NSData*)deviceToken{
    const char *data = [deviceToken bytes];
    
    NSMutableString *token = [NSMutableString string];
    for(int i = 0; i < [deviceToken length]; i++) {
        [token appendFormat:@"%02.2hhx", data[i]];
    }
    
    return [token copy];
}

@end
