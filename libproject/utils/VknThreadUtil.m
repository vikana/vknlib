//
//  VknThreadUtil.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknThreadUtil.h"

@implementation VknThreadUtil


+(void) threadBlock:(VknThreadBlock_t)block{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block);
}

+(void) mainBlock:(VknThreadBlock_t)block{
    dispatch_async(dispatch_get_main_queue(), block);
}

+(void) threadBlock:(VknThreadBlock_t)block
           callback:(VknThreadBlock_t)callback{
    [self threadBlock:^{
        block();
        [self mainBlock:callback];
    }];
}

@end
