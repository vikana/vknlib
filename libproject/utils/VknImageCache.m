//
//  VknImageCache.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknImageCache.h"
#import "VknUrlLoadOperation.h"

@implementation VknImageCache

static NSMutableDictionary* cacheList;
static NSOperationQueue* queue;

//+(void) create{
//    cacheList = [[NSMutableDictionary alloc] init];
//    queue = [[NSOperationQueue alloc] init];
//}

+(void) clear{
    @synchronized(cacheList){
        [cacheList removeAllObjects];
        [queue cancelAllOperations];
    }
}

+(void) clear:(NSString*)key{
    @synchronized(cacheList){
        [cacheList removeObjectForKey:key];
    }
}

+(BOOL) hasCache:(NSString*)url{
    @synchronized(cacheList){
        return ([cacheList objectForKey:url] != nil);
    }
}

+(UIImage*) getChache:(NSString*)url{
    if(![self hasCache:url]){
        return nil;
    }
    
    @synchronized(cacheList){
        return [cacheList objectForKey:url];
    }
}

+(void) setCache:(UIImage*)image forKey:(NSString*)url{
    @synchronized(cacheList){
        [cacheList setObject:image forKey:url];
    }
    
}

static int loadingNum = 0;
+(void) loadImage:(NSString*)url defaultImage:(UIImage *)image callback:(onload_image_t)callback{
    //    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // TODO nullチェックのコストはどうなんだろう +create() つかうか
        if(cacheList == nil){
            cacheList = [[NSMutableDictionary alloc] init];
            queue = [[NSOperationQueue alloc] init];
        }
        
        loadingNum++;
        // キャッシュがあればそれ使って終わり
        if([self hasCache:url]){
            callback([cacheList objectForKey:url], url, YES);
            loadingNum--;
            return;
        }
        
        VknUrlLoadOperation* op = [[VknUrlLoadOperation alloc] init];
        [op setUrl:url];
        op.onFinished = ^(VknUrlLoadOperation* operation, NSData* data){
            loadingNum--;
            UIImage* image = [UIImage imageWithData:data];
            if(image == nil) return;
            [self setCache:image forKey:url];
            callback(image, url, NO);
        };
        op.onError = ^(VknUrlLoadOperation* operation, NSError* error){
            loadingNum--;
        };
        [queue addOperation:op];
        
    });
}

@end
