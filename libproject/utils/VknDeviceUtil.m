//
//  VknDeviceUtil.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknDeviceUtil.h"

@implementation VknDeviceUtil

+(BOOL) is35Inch{
    return ([self windowHeight] < 568);
}

+(float)windowHeight{
    return [[UIScreen mainScreen] bounds].size.height;
}

@end
