//
//  VknUtils.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VknColorUtil.h"
#import "VknDataUtil.h"
#import "VknDateUtil.h"
#import "VknDeviceUtil.h"
#import "VknImageCache.h"
#import "VknImageUtil.h"
#import "VknThreadUtil.h"
#import "VknFileUtil.h"

typedef void (^VknUtilsCallback_t)();

@interface VknUtils : NSObject

+ (NSUInteger)randIntRange:(NSRange)range;
+ (NSString*)appStoreUrl:(NSString*) appId;
+(BOOL) isNull:(id)obj;
+(BOOL) isNotNull:(id)obj;

+ (UInt64)getEpochSeconds;
+ (UInt64)getEpochMilliSeconds;

+(int) randInt:(int)max;

+(void) openBrowser:(NSString*)url;
+(void) wait:(float)millisec callback:(VknUtilsCallback_t)callback;
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end


@interface NSArray(VknArray)

-(NSArray *)shuffled;

@end