//
//  VknFileUtil.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknFileUtil.h"

@implementation VknFileUtil


static NSFileManager* fm;

+(NSString*) cachePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    return path;
}

+(NSString*)documentPath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(
                                                         NSDocumentDirectory
                                                         , NSUserDomainMask
                                                         , YES
                                                         );
    return [paths objectAtIndex:0];
}

+(NSString*) documentFilePath:(NSString*)fileName{
    return [NSString stringWithFormat:@"%@/%@", [self documentPath], fileName];
}

+(NSString*)cacheDir{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    
    return path;
}

+(NSString*)cacheFilePath:(NSString*)fileName{
    return [NSString stringWithFormat:@"%@/%@", [self cacheDir], fileName];
}

+(NSString*) createCachePath:(NSString *)path{
    return [NSString stringWithFormat:@"%@/%@", [self cachePath], path];
}

+(BOOL) isExists:(NSString *)path{
    NSFileManager* fileManager = [[NSFileManager alloc] init];
    return [fileManager fileExistsAtPath:path];
}

+(void)mkDir:(NSString*)dirPath{
    if([self isExists:dirPath]) return;
    
    // ファイルマネージャを作成
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    
    // ディレクトリを作成
    [fileManager createDirectoryAtPath:dirPath
           withIntermediateDirectories:YES
                            attributes:nil
                                 error:&error];
    
    if(error){
        NSLog(@"%@", error.debugDescription);
    }else{
        NSLog(@"createSuccess:%@", dirPath);
    }
}

+(void)mkDirInCache:(NSString*)path{
}

+(NSString*)tmpPath{
    NSString *path = NSTemporaryDirectory();
    return path;
}

+(NSString*) tmpFilePath:(NSString*)fileName{
    return [NSString stringWithFormat:@"%@%@", [self tmpPath], fileName];
}

@end
