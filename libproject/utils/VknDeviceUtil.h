//
//  VknDeviceUtil.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VknDeviceUtil : NSObject

+(BOOL) is35Inch;
+(float)windowHeight;

@end
