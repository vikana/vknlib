//
//  AbstractDBManager.m
//  herplife
//
//  Created by 河瀬 悠 on 2014/01/07.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "AbstractVknDBManager.h"
#import "VknFileUtil.h"

@implementation AbstractVknDBManager

-(FMDatabase*) db{
    return [[FMDatabase alloc] initWithPath:[self dbFilePath]];
}

-(FMDatabaseQueue*)dbQueue{
    return [[FMDatabaseQueue alloc] initWithPath:[self dbFilePath]];
}

-(NSString*) dbFilePath{
    return [VknFileUtil documentFilePath:[self dbFileName]];
}

-(BOOL) hasError:(FMDatabase*)db{
    return ([db lastErrorCode] != 0);
}

-(void) showError:(FMDatabase*)db{
    if(![self hasError:db]) return;
    NSLog(@"dbError:%d: %@", [db lastErrorCode], [db lastErrorMessage]);
}

-(void) insertData:(NSDictionary*)insertList db:(FMDatabase*)db{
    NSArray* keyList = [insertList allKeys];
    for(NSString* key in keyList){
        NSMutableArray* columnList = [[self columnList:key] mutableCopy];
        [columnList addObject:@"id"];
        NSArray* list = [insertList objectForKey:key];
        NSString* sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)",
                         key, [columnList componentsJoinedByString:@","]
                         , [[self placeHolderWithNum:[columnList count]] componentsJoinedByString:@","]
                         ];
        for(NSDictionary* row in list){
            NSMutableArray* values = [[NSMutableArray alloc] init];
            for(int i=0,max=[columnList count];i<max;i++){
                NSString* colum = [columnList objectAtIndex:i];
                [values addObject:[row objectForKey:colum]];
            }
            
            [db executeUpdate:sql withArgumentsInArray:values];
            [self showError:db];
        }
    }
}

-(NSArray*) placeHolderWithNum:(int)num{
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    for(int i=0;i<num;i++){
        [tmp addObject:@"?"];
    }
    
    return [NSArray arrayWithArray:tmp];
}

-(void) updateData:(NSDictionary*)updateList db:(FMDatabase*)db{
    NSArray* keyList = [updateList allKeys];
    for(NSString* key in keyList){
        NSMutableString* ms = [[NSMutableString alloc] init];
        [ms appendFormat:@"UPDATE %@ SET ", key];
        NSArray* columnList = [self columnList:key];
        for(int i=0,max=[columnList count];i<max;i++){
            if(i != 0)[ms appendString:@","];
            NSString* colum = [columnList objectAtIndex:i];
            [ms appendFormat:@" %@ = ? ", colum];
        }
        
        [ms appendString:@" WHERE id = ?"];
        NSString* sql = [NSString stringWithString:ms];
        
        NSArray* list = [updateList objectForKey:key];
        for(NSDictionary* row in list){
            NSMutableArray* values = [[NSMutableArray alloc] init];
            for(int i=0,max=[columnList count];i<max;i++){
                NSString* colum = [columnList objectAtIndex:i];
                [values addObject:[row objectForKey:colum]];
            }
            [values addObject:[row objectForKey:@"id"]];
            
            [db executeUpdate:sql withArgumentsInArray:values];
            [self showError:db];
        }
    }
}

-(void) deleteData:(NSDictionary*)deleteList db:(FMDatabase*)db{
    NSArray* keyList = [deleteList allKeys];
    for(NSString* key in keyList){
        NSArray* list = [deleteList objectForKey:key];
        for(NSDictionary* row in list){
            NSNumber* deleteId = [row objectForKey:@"id"];
            NSString* sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE id = ?", key];
            [db executeUpdate:sql, deleteId];
            [self showError:db];
        }
    }
}

-(void) updateData:(NSDictionary *)json callback:(AbstractDBManagerBOOLCallback_t)callback{
    [[self dbQueue] inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSDictionary* created = [json objectForKey:@"created"];
        [self insertData:created db:db];
        
        NSDictionary* updated = [json objectForKey:@"updated"];
        [self updateData:updated db:db];
        
        NSDictionary* deleted = [json objectForKey:@"deleted"];
        [self deleteData:deleted db:db];
        
        callback(YES);
    }];
}

#pragma mark -- abstract --

-(NSString*)dbFileName{
    return @"vkn.db";
}

-(NSArray*) columnList:(NSString*)tableName{
    return @[];
}

@end
