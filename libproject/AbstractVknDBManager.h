//
//  AbstractDBManager.h
//  herplife
//
//  Created by 河瀬 悠 on 2014/01/07.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

typedef void (^AbstractDBManagerBOOLCallback_t)(BOOL isSuccess);

@interface AbstractVknDBManager : NSObject

-(FMDatabase*) db;
-(FMDatabaseQueue*)dbQueue;
-(NSString*) dbFilePath;
-(BOOL) hasError:(FMDatabase*)db;
-(void) showError:(FMDatabase*)db;
-(void) updateData:(NSDictionary *)json callback:(AbstractDBManagerBOOLCallback_t)callback;

@end
