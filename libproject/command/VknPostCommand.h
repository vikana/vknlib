//
//  VknPostCommand.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknCommand.h"
#import "R9HTTPRequest.h"

/**---------------------------------------------
 @brief POSTするコマンド
 @note
 
 */
@interface VknPostCommand : AbstractVknCommand{
    NSString* url;
    NSDictionary* values;
}


-(id) initWithValues:(NSDictionary*)values_ withUrl:(NSString*)url_;
-(id) initWithUrl:(NSString*)url_;

-(void) setValues:(NSDictionary*)values_;
- (void)setData:(NSData *)data
   withFileName:(NSString *)fileName
 andContentType:(NSString *)contentType
         forKey:(NSString *)key;

@end
