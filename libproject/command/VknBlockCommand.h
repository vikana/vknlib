//
//  VknBlockCommand.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknCommand.h"

typedef NSData* (^VknCommandBlock_t)(NSData* data);

/**---------------------------------------------
 @brief ブロックコマンド
 @note
 
 */
@interface VknBlockCommand : AbstractVknCommand{
    VknCommandBlock_t block;
}

-(id) initWithBlock:(VknCommandBlock_t)block_;
+(NSData*)createFailData;


@end