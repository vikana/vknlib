//
//  AbstractVknExecutor.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknExecutor.h"

@implementation AbstractVknExecutor

@synthesize onFail;
@synthesize onComplete;
@synthesize onProgress;

-(id)init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(id) initWithCommandList:(VknCommandList*)commandList_{
    self = [super init];
    if(self){
        commandList = commandList_;
    }
    
    return self;
}

// initialize ===================

-(void) initialize{
    commandList = [[VknCommandList alloc] init];
}

// private =====================

-(void) assignDelegate{
    
    for(int i=0,max=[commandList count];i<max;i++){
        NSObject<IVknCommand>* command = [commandList commandAtIndex:i];
        [command setDelegate:self];
    }
}

-(BOOL) hasNext{
    return (current < total);
}

-(void) tryNext:(NSData*) data{
    if(![self hasNext]){
        [delegate onComplete:self data:data];
    }else{
        NSObject<IVknCommand>* command = [commandList commandAtIndex:current];
        current++;
        [command execute:data];
    }
}

// SLIExecutor =================

-(void) onComplete:(NSObject<IVknCommand>*)command data:(NSData*)data{
    [self tryNext:data];
}

-(void) onCancle:(NSObject<IVknCommand>*)command{
    // TODO cancelAll
    
}

-(void) onError:(NSObject<IVknCommand>*)command error:(NSError*)error{
    // TODO abort
    NSLog(@"%@", error);
    [delegate onError:command error:error];
    if(onFail){
        NSLog(@"exists onFail block");
        onFail(error);
    }
}

-(void) onProgress:(NSObject<IVknCommand> *)command progress:(float)progress{
    
}

// override ============================

-(void) internalExecute:(NSData *)data{
    total = [commandList count];
    current = 0;
    [self assignDelegate];
    [self tryNext:data];
}

// ▼ public -==========================

-(void) push:(NSObject<IVknCommand>*)command{
    [commandList push:command];
}

@end
