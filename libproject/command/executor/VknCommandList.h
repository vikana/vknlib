//
//  VknCommandList.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IVknCommand.h"

@interface VknCommandList : NSObject{
    NSMutableArray* list;
}

-(id) initWithArray:(NSArray*)array;
-(int) count;
-(void) push:(NSObject<IVknCommand>*)command;
-(NSObject<IVknCommand>*) commandAtIndex:(int)index;
-(void) removeAtIndex:(int)index;

@end