//
//  VknSerialExecutor.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknSerialExecutor.h"


@implementation VknSerialExecutor

@synthesize onComplete;
@synthesize onProgress;
@synthesize onFail;

// override ============================

-(void) internalExecute:(NSData *)data{
    progressList = [[NSMutableArray alloc] init];
    for(int i=0,max=[commandList count];i<max;i++){
        NSObject<IVknCommand>* c = [commandList commandAtIndex:i];
        c.tag = i;
        [progressList addObject:[NSNumber numberWithFloat:0.0f]];
    }
    
    __weak VknSerialExecutor* SELF = self;
    
    [commandList commandAtIndex:[commandList count]-1].onComplete = ^(NSData* data){
        [SELF onComplete:data];
        if(onComplete)
            onComplete(data);
    };
    
    [super internalExecute:data];
}

// Override
-(void) onProgress:(NSObject<IVknCommand> *)command progress:(float)progress{
    
    [progressList replaceObjectAtIndex:command.tag withObject:[NSNumber numberWithFloat:progress]];
    
    float totalProgress = 0;
    for(int i=0,max=[progressList count];i<max;i++){
        totalProgress += [[progressList objectAtIndex:i] floatValue];
    }
    
    float p = totalProgress / (float) total;
    [delegate onProgress:self progress:p];
    
    if(onProgress)
        onProgress(p);
}

// Override
-(void) onError:(NSObject<IVknCommand>*)command error:(NSError*)error{
    [super onError:command error:error];
    if(onFail)
        onFail(error);
    //    [self tryNext:nil];
}

@end
