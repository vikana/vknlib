//
//  VknParallelExecutor.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknExecutor.h"

/**---------------------------------------------
 @brief 並列実行
 @note
 
 */
@interface VknParallelExecutor : AbstractVknExecutor

@end
