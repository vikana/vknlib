//
//  IVknExecutor.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "IVknCommand.h"
#import "VknCommandList.h"

@protocol IVknExecutor <NSObject, VknCommandDelegate, IVknCommand>

-(void) push:(NSObject<IVknCommand>*)command;
-(id) initWithCommandList:(VknCommandList*)commandList_;

@end

