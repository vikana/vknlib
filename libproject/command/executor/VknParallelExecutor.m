//
//  VknParallelExecutor.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknParallelExecutor.h"

@implementation VknParallelExecutor

@synthesize onComplete;
@synthesize onFail;
@synthesize onProgress;

static NSObject* lock;
static NSOperationQueue* operationQueue;


-(void) tryNext:(NSData*)data{
    if([commandList count] == 0){
        [self onComplete:data];
        return;
    }
    
    for(int i=0,max=[commandList count];i<max;i++){
        NSObject<IVknCommand>* command = [commandList commandAtIndex:i];
        [command execute:nil];
    }
    
}

// Override
-(void) onComplete:(NSObject<IVknCommand>*)command data:(NSData *)data{
    BOOL hasNext = YES;
    @synchronized (lock){
        current++;
        hasNext = (current >= total);
        if(hasNext){
            if(onComplete)
                onComplete(data);
            [delegate onComplete:self data:data];
        }
    }
}

// Override
-(void) onProgress:(NSObject<IVknCommand> *)command progress:(float)progress{
    [progressList replaceObjectAtIndex:command.tag withObject:[NSNumber numberWithFloat:progress]];
    
    float totalProgress = 0;
    for(int i=0,max=[progressList count];i<max;i++){
        totalProgress += [[progressList objectAtIndex:i] floatValue];
    }
    
    float p = totalProgress / (float) total;
    if(onProgress)
        onProgress(p);
    [delegate onProgress:self progress:p];
}

// Override
-(void) internalExecute:(NSObject *)data{
    if(lock == nil){
        lock = [[NSObject alloc] init];
    }
    if(operationQueue){
        operationQueue = [[NSOperationQueue alloc] init];
    }
    
    progressList = [[NSMutableArray alloc] init];
    for(int i=0,max=[commandList count];i<max;i++){
        NSObject<IVknCommand>* c = [commandList commandAtIndex:i];
        c.tag = i;
        [progressList addObject:[NSNumber numberWithFloat:0.0f]];
    }
    
    [super internalExecute:data];
}

@end
