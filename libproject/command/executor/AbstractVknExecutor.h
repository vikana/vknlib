//
//  AbstractVknExecutor.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknCommand.h"
#import "IVknExecutor.h"

/**---------------------------------------------
 @brief エクスキューター
 @note
 
 */
@interface AbstractVknExecutor : AbstractVknCommand<IVknExecutor>{
    VknCommandList* commandList;
    NSMutableArray* progressList;
    int total;
    int current;
}

-(void) tryNext:(NSData*) data;
-(BOOL) hasNext;

@end
