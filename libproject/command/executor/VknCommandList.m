//
//  VknCommandList.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknCommandList.h"

@implementation VknCommandList

-(id) init{
    self = [super init];
    if(self){
        [self initialize];
    }
    
    return self;
}

-(id) initWithArray:(NSArray*)array{
    self = [super init];
    if(self){
        list = [[NSMutableArray alloc] initWithArray:array];
    }
    
    return self;
}

-(void) initialize{
    list = [[NSMutableArray alloc] init];
}

// ▼ public =================================

-(int) count{
    return [list count];
}

-(void) push:(NSObject<IVknCommand>*)command{
    [list addObject:command];
}

-(void) removeAtIndex:(int)index{
    [list removeObjectAtIndex:index];
}

-(NSObject<IVknCommand>*) commandAtIndex:(int)index{
    NSObject<IVknCommand>* command = [list objectAtIndex:index];
    
    // TODO error
    return command;
}

@end
