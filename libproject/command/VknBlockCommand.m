//
//  VknBlockCommand.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknBlockCommand.h"

@implementation VknBlockCommand

-(id) initWithBlock:(VknCommandBlock_t)block_{
    self = [super init];
    if(self){
        block = block_;
    }
    
    return self;
}

static NSData* failData;
+(NSData*)createFailData{
    
    @synchronized(failData){
        if(failData == nil){
            failData = [[NSData alloc] init];
        }
    }
    
    return failData;
}

// override
-(void) internalExecute:(NSData*)data{
    // progressはなしで
    [self onProgress:0];
    [self onProgress:1];
    
    NSData* newData = block(data);
    
    if(newData == [VknBlockCommand createFailData]){
        [delegate onError:self error:[NSError errorWithDomain:@"VknBlockCommand fail" code:1 userInfo:nil]];
    }else{
        [self onComplete:newData];
    }
    
}

@end
