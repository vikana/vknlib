//
//  AbstractVknCommand.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknCommand.h"

@implementation AbstractVknCommand
@synthesize onComplete;
@synthesize onProgress;
@synthesize onFail;
@synthesize tag;

-(void) setDelegate:(NSObject<VknCommandDelegate> *)delegate_{
    delegate = delegate_;
}

// abstract =====================

-(void) internalExecute:(NSData*)data{
    [NSException raise:@"noMethodException" format:@"internalExecuteを実装して"];
}

-(void) onComplete:(NSData*)data{
    [delegate onComplete:self data:data];
    if(onComplete)
        onComplete(data);
}

-(void) onProgress:(float)progress{
    [delegate onProgress:self progress:progress];
    if(onProgress)
        onProgress(progress);
}

-(void) onCancel{
    [delegate onCancle:self];
}

-(void) onError:(NSError*)error{
    [delegate onError:self error:error];
    if(onFail)
        onFail(error);
}

// public =======================

-(void) execute:(NSObject*)data{
    [self internalExecute:data];
}

-(void) cancel{
    
}

@end

