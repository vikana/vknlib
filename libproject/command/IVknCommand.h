//
//  IVknCommand.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

// blocksインタフェース
typedef void (^onVknCommandComplete_t)(NSData* data);
typedef void (^onVknCommandProgress_t)(float progress);
typedef void (^onVknCommandFail_t)(NSError* error);


@protocol IVknCommand;

// delegateインタフェース
@protocol VknCommandDelegate

@optional
-(void) onComplete:(NSObject<IVknCommand>*)command data:(NSData*)data;
-(void) onCancle:(NSObject<IVknCommand>*)command;
-(void) onProgress:(NSObject<IVknCommand>*)command progress:(float)progress;
-(void) onError:(NSObject<IVknCommand>*)command error:(NSError*)error;

@end


// これがmain

@protocol IVknCommand <NSObject>

-(void) setDelegate:(NSObject<VknCommandDelegate>*)delegate_;
-(void) execute:(NSData*)data;
-(void) cancel;

@property (strong) onVknCommandComplete_t onComplete;
@property (strong) onVknCommandProgress_t onProgress;
@property (strong) onVknCommandFail_t onFail;
@property int tag;

@end
