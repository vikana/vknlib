//
//  VknWaitCommand.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknWaitCommand.h"

@implementation VknWaitCommand

-(id) initWithDelay:(float)delay_{
    self = [super init];
    if(self){
        delay = delay_;
    }
    
    return self;
}

-(void) internalExecute:(NSObject *)data{
    [VknThreadUtil threadBlock:^{
        [NSThread sleepForTimeInterval:delay];
    } callback:^{
        [self onComplete:nil];
    }];
}

@end
