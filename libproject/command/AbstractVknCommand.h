//
//  AbstractVknCommand.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IVknCommand.h"

@interface AbstractVknCommand : NSObject<IVknCommand>{
    NSObject<VknCommandDelegate>* delegate;
    int tag;
}

// abstract ==================
-(void) internalExecute:(NSObject*) data;
-(void) onComplete:(NSObject*)data;
-(void) onProgress:(float)progress;
-(void) onCancel;
-(void) onError:(NSError*)error;

@end
