//
//  VknPostCommand.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknPostCommand.h"

@interface VknPostCommand (){
    R9HTTPRequest* request;
}

@end

@implementation VknPostCommand
@synthesize onComplete;
@synthesize onProgress;
@synthesize onFail;

#pragma mark -- initialize --

-(id) initWithUrl:(NSString *)url_{
    self = [super init];
    if(self){
        url = url_;
        [self setup];
    }
    
    return self;
}

-(id) initWithValues:(NSDictionary *)values_ withUrl:(NSString *)url_{
    self = [self initWithUrl:url_];
    if(self){
        values = values_;
    }
    
    return self;
}

-(void) setup{
    NSURL* nsurl = [NSURL URLWithString:url];
    request = [[R9HTTPRequest alloc] initWithURL:nsurl];
    [request setHTTPMethod:@"POST"];
}

#pragma mark -- override --

-(void) internalExecute:(NSData *)data{
    
    for(NSString* key in values){
        id value = [values objectForKey:key];
        [request addBody:value forKey:key];
    }
    
    __block VknPostCommand* SELF = self;
    [request setCompletionHandler:^(NSHTTPURLResponse* responseHeader, NSString* responseString){
        NSData* responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        if(SELF.onComplete)
            SELF.onComplete(responseData);
        [SELF onComplete:responseData];
    }];
    [request setUploadProgressHandler:^(float progress){
        [SELF onProgress:progress];
    }];
    [request setFailedHandler:^(NSError* error){
        if(SELF.onFail)
            SELF.onFail(error);
        [SELF onError:error];
    }];
    
    [request startRequest];
}

#pragma mark -- public --

-(void) setValues:(NSDictionary *)values_{
    values = values_;
}

#pragma mark -- delegation --

- (void)setData:(NSData *)data
   withFileName:(NSString *)fileName
 andContentType:(NSString *)contentType
         forKey:(NSString *)key{
    [request setData:data withFileName:fileName andContentType:contentType forKey:key];
}


@end
