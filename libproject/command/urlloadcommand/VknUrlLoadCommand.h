//
//  VknUrlLoadCommand.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknCommand.h"
#import "VknUrlLoadOperation.h"

/**---------------------------------------------
 @brief URLからデータを取ってくるコマンド
 @note
 */
@interface VknUrlLoadCommand : AbstractVknCommand{
    VknUrlLoadOperation* urlLoadOperation;
    BOOL loading;
}

-(id)initWithUrl:(NSString*)url;
-(BOOL)isLoading;

@end
