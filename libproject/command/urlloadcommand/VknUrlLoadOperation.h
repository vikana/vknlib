//
//  VknUrlLoadOperation.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VknUrlLoadOperation;

typedef void (^onVknUrlLoadOperationProgress_t)(VknUrlLoadOperation* operation, float progress);
typedef void (^onVknUrlLoadOperationFinished_t)(VknUrlLoadOperation* operation, NSData* data);
typedef void (^onVknUrlLoadOperationError_t)(VknUrlLoadOperation* operation, NSError* error);

@protocol VknUrlLoadOperationDelegate<NSObject>

-(void)onProgress:(VknUrlLoadOperation*)operation progress:(float)progress;
-(void)onFinished:(VknUrlLoadOperation*)operation data:(NSData*)data;
-(void)onError:(VknUrlLoadOperation*)operation  error:(NSError*)error;

@end

/**---------------------------------------------
 @brief URLLoadOperationのラッパー
 @note
 
 */

@interface VknUrlLoadOperation : NSOperation<NSURLConnectionDataDelegate>{
    NSMutableURLRequest* urlRequest;
    BOOL isFinished;
    BOOL isExecuting;
    float totalBytes;
    float loadedBytes;
    
    // callback
    onVknUrlLoadOperationProgress_t onProgress;
    onVknUrlLoadOperationFinished_t onFinished;
    onVknUrlLoadOperationError_t onError;
    
    __weak NSObject<VknUrlLoadOperationDelegate>* callbackObject;
    NSString* url;
    NSMutableData* reseivedData;
}

@property (copy, nonatomic) onVknUrlLoadOperationProgress_t onProgress;
@property (copy, nonatomic) onVknUrlLoadOperationFinished_t onFinished;
@property (copy, nonatomic) onVknUrlLoadOperationError_t onError;

-(id) initWithCallbackObject:(NSObject<VknUrlLoadOperationDelegate>*)callbackObject_ withUrl:(NSString*)url_;
-(void) setCallbackObject:(NSObject<VknUrlLoadOperationDelegate>*)callbackObject_;
-(void) setUrl:(NSString*)url_;
-(NSString*) getUrl;
-(NSString*) getId;

// protected
-(void) initialize;

@end
