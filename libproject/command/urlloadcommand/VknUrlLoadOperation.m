//
//  VknUrlLoadOperation.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknUrlLoadOperation.h"
#define TIME_OUT 20

@implementation VknUrlLoadOperation

@synthesize onError;
@synthesize onProgress;
@synthesize onFinished;

// 参考
// http://d.hatena.ne.jp/glass-_-onion/20110706/1309909082
// http://blog.cloud-study.com/?p=17


+ (BOOL)automaticallyNotifiesObserversForKey:(NSString*)key {
    if ([key isEqualToString:@"isExecuting"] ||
        [key isEqualToString:@"isFinished"]) {
        return YES;
    }
    return [super automaticallyNotifiesObserversForKey:key];
}

-(id) init{
    self = [super init];
    if(self){
        reseivedData = [[NSMutableData alloc] init];
        [self initialize];
    }
    
    return self;
}

// protected
-(void) initialize{}

-(id) initWithCallbackObject:(NSObject<VknUrlLoadOperationDelegate>*)callbackObject_ withUrl:(NSString*)url_{
    self = [self init];
    if(self){
        callbackObject = callbackObject_;
        url = url_;
    }
    
    return self;
}

-(void) setCallbackObject:(NSObject<VknUrlLoadOperationDelegate>*)callbackObject_{
    callbackObject = callbackObject_;
}

-(void) setUrl:(NSString*)url_{
    url = url_;
}

-(NSString*) getUrl{
    return url;
}

-(NSString*) getId{
    // TODO
    return @"";
}

-(void)start{
    
    urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [urlRequest setTimeoutInterval:TIME_OUT]; // TODO タイムアウトは設定できるように
    NSURLConnection *conn = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
    if(!conn){
        // error
        return;
    }
    [self setValue:[NSNumber numberWithBool:YES] forKey:@"isExecuting"];
    [self setValue:[NSNumber numberWithBool:NO] forKey:@"isFinished"];
    
    while (![self isFinished] && ![self isCancelled]) {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2f]];
    }
}

// NSURLConnectionDataDelegate ==========================

- (NSURLRequest *)connection:(NSURLConnection *)connection
             willSendRequest:(NSURLRequest *)request
            redirectResponse:(NSURLResponse *)response{
    return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    totalBytes = [response expectedContentLength];
    loadedBytes = 0.0;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [reseivedData appendData:data];
    loadedBytes += [data length];
    float progress = loadedBytes/totalBytes;
    [callbackObject onProgress:self progress:progress];
    
    if(onProgress != nil)
        onProgress(self, progress);
    
}

- (void)connection:(NSURLConnection *)connection
   didSendBodyData:(NSInteger)bytesWritten
 totalBytesWritten:(NSInteger)totalBytesWritten
totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
    return cachedResponse;
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    [self setValue:[NSNumber numberWithBool:NO] forKey:@"isExecuting"];
    [self setValue:[NSNumber numberWithBool:YES] forKey:@"isFinished"];
    [callbackObject onError:self error:error];
    
    if(onError != nil)
        onError(self, error);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self setValue:[NSNumber numberWithBool:NO] forKey:@"isExecuting"];
    [self setValue:[NSNumber numberWithBool:YES] forKey:@"isFinished"];
    [callbackObject onFinished:self data:reseivedData];
    
    if(onFinished != nil)
        onFinished(self, reseivedData);
    
    // TODO ? このクラス自身が解放されてないのかも
    reseivedData = nil;
}

// NSOperation =================================

//-(BOOL) isConcurrent{
//    return YES;
//}

-(BOOL) isExecuting{
    return isExecuting;
}
//
-(BOOL) isFinished{
    return isFinished;
}

@end