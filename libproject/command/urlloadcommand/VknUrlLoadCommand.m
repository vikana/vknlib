//
//  VknUrlLoadCommand.m
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknUrlLoadCommand.h"

@implementation VknUrlLoadCommand

@synthesize onProgress;
@synthesize onComplete;
@synthesize onFail;

#define MAX_OPERATION_COUNT 10

static NSOperationQueue* queue;

-(id) initWithUrl:(NSString *)url{
    self = [super init];
    if(self){
        [self initialize:url];
    }
    
    return self;
}

-(void) initialize:(NSString*)url{
    @synchronized([VknUrlLoadCommand class]){
        if(queue == nil){
//            int numOfCores = [[NSProcessInfo processInfo] processorCount];
            queue = [[NSOperationQueue alloc] init];
            [queue setMaxConcurrentOperationCount:MAX_OPERATION_COUNT];
        }
    }
    
    loading = NO;
    urlLoadOperation = [[VknUrlLoadOperation alloc] init];
    [urlLoadOperation setUrl:url];
    
    __weak VknUrlLoadCommand* SELF = self;
    urlLoadOperation.onFinished = ^(VknUrlLoadOperation* operation, NSData* data){
        [SELF onComplete:data];
    };
    urlLoadOperation.onError = ^(VknUrlLoadOperation* operation, NSError* error){
        [SELF onError:error];
    };
    urlLoadOperation.onProgress = ^(VknUrlLoadOperation* operation, float progress){
        [SELF onProgress:progress];
    };
}

-(void) setOnComplete:(onVknCommandComplete_t)onComplete_{
    __weak VknUrlLoadCommand* SELF = self;
    urlLoadOperation.onFinished = ^(VknUrlLoadOperation* operation, NSData* data){
        onComplete_(data);
        [SELF onComplete:data];
        
    };
}

-(void) setOnProgress:(onVknCommandProgress_t)onProgress_{
    __weak VknUrlLoadCommand* SELF = self;
    urlLoadOperation.onProgress = ^(VknUrlLoadOperation* operation, float progress){
        onProgress_(progress);
        [SELF onProgress:progress];
    };
}

-(void) setOnFail:(onVknCommandFail_t)onFail_{
    __weak VknUrlLoadCommand* SELF = self;
    urlLoadOperation.onError = ^(VknUrlLoadOperation* operation, NSError* error){
        onFail_(error);
        [SELF onError:error];
    };
}

-(BOOL) isLoading{
    return loading;
}

// Override
-(void) internalExecute:(NSData*)data{
    loading = YES;
    [queue addOperation:urlLoadOperation];
}

@end
