//
//  VknWaitCommand.h
//  libproject
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "AbstractVknCommand.h"
#import "VknThreadUtil.h"

/**---------------------------------------------
 @brief waitコマンド
 @note
 
 */
@interface VknWaitCommand : AbstractVknCommand{
    float delay;
}

-(id) initWithDelay:(float)delay_;

@end
