//
//  VknViewController.m
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import "VknViewController.h"
#import "Tween.h"
#import "VknThreadUtil.h"

@interface VknViewController (){
    BOOL isLoading;
    UIView* loadingView;
    UIView* toastView;
}

@end

@implementation VknViewController

-(void) showLoading{
    if(isLoading) return;
    isLoading = YES;
    [self createLoadingView];
    
    // animation
    loadingView.alpha = 0;
    loadingView.transform = CGAffineTransformMakeScale(2.0f, 2.0f);
    [Tween addTween:self tweenId:0 startValue:0 endValue:1.0 time:0.2f delay:0 easing:@"easeInQuint" startSEL:@selector(updateShow:) updateSEL:@selector(updateShow:) endSEL:@selector(updateShow:)];
}

-(void)updateShow:(TweenObject*)tween{
    double val = tween.currentValue;
    
    loadingView.alpha = val;
    double scale = 2.0 - (1.0 * val);
    
    loadingView.transform = CGAffineTransformMakeScale(scale, scale);
}

-(void) createLoadingView{
    CGRect frame = CGRectMake(0, 0, 100, 100);
    loadingView = [[UIView alloc] initWithFrame:frame];
    loadingView.layer.cornerRadius = 10;
    loadingView.clipsToBounds = YES;
    UIView* bgView = [[UIView alloc] initWithFrame:frame];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.5f;
    [loadingView addSubview:bgView];
    
    UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.frame = frame;
    [loadingView addSubview:indicator];
    [indicator startAnimating];
    
    UIView* view = self.view;
    loadingView.center = view.center;
    [view addSubview:loadingView];
}

-(void) hideLoading{
    [Tween addTween:self tweenId:1 startValue:0 endValue:1.0 time:0.2f delay:0 easing:@"easeInQuint" startSEL:@selector(updateHide:) updateSEL:@selector(updateHide:) endSEL:@selector(endHide:)];
}

-(void) updateHide:(TweenObject*)tween{
    double val = tween.currentValue;
    double alpha = 1.0 - val;
    loadingView.alpha = alpha;
    
    double y = -20 * val;
    loadingView.transform = CGAffineTransformMakeTranslation(0, y);
}

-(void) endHide:(TweenObject*)tween{
    [self updateHide:tween];
    [loadingView removeFromSuperview];
    isLoading = NO;
}


-(void) showToast:(NSString *)message{
    if(toastView){
        [toastView removeFromSuperview];
    }
    CGRect frame = CGRectMake(0, 0, 250, 150);
    toastView = [[UIView alloc] initWithFrame:frame];
    toastView.layer.cornerRadius = 10;
    toastView.clipsToBounds = YES;
    toastView.backgroundColor = [UIColor clearColor];
    
    UIView* bgView = [[UIView alloc] initWithFrame:frame];
    bgView.backgroundColor = [UIColor blackColor];
    bgView.alpha = 0.75f;
    [toastView addSubview:bgView];

    UILabel* messageLabel = [[UILabel alloc] initWithFrame:frame];
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.text = message;
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.backgroundColor = [UIColor clearColor];
    [messageLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideToast)]];
    messageLabel.userInteractionEnabled = YES;
    [toastView addSubview:messageLabel];
    
    UIView* view = self.view;
    toastView.center = view.center;
    [view addSubview:toastView];
    
    // animation
    toastView.alpha = 0;
    toastView.transform = CGAffineTransformMakeTranslation(0, 20);
    [Tween addTween:self tweenId:2 startValue:0 endValue:1.0 time:0.3f delay:0 easing:@"easeOutQuint" startSEL:@selector(updateShowToast:) updateSEL:@selector(updateShowToast:) endSEL:@selector(updateShowToast:)];
    
    [VknThreadUtil threadBlock:^{
        [NSThread sleepForTimeInterval:2.0f];
    } callback:^{
        [self hideToast];
    }];
}

-(void) hideToast{
    if(toastView.alpha != 1.0) return;
    [Tween addTween:self tweenId:3 startValue:0 endValue:1.0 time:0.2f delay:0 easing:@"easeInQuint" startSEL:@selector(updateHideToast:) updateSEL:@selector(updateHideToast:) endSEL:@selector(endHideToast:)];
}

-(void) updateShowToast:(TweenObject*)tween{
    double val = tween.currentValue;
    toastView.alpha = val;
    double y = 20 - (20 * val);
    toastView.transform = CGAffineTransformMakeTranslation(0, y);
}

-(void) updateHideToast:(TweenObject*)tween{
    double val = tween.currentValue;
    double alpha = 1.0 - val;
    
    toastView.alpha = alpha;
    toastView.transform = CGAffineTransformMakeTranslation(0, -20.0*val);
}

-(void) endHideToast:(TweenObject*)tween{
    [self updateHideToast:tween];
    [toastView removeFromSuperview];
}

-(void) dealloc{
    NSLog(@"dealloc");
    [Tween removeTweenForId:0];
    [Tween removeTweenForId:1];
    [Tween removeTweenForId:2];
    [Tween removeTweenForId:3];
}

@end
