//
//  VknViewController.h
//  nurse
//
//  Created by 河瀬 悠 on 2014/02/27.
//  Copyright (c) 2014年 vikana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VknViewController : UIViewController

-(void) showLoading;
-(void) hideLoading;
-(void) showToast:(NSString*)message;

@end
