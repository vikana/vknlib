//
//  VknModel.m
//  salon1
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknModel.h"

@implementation VknModel

@synthesize id_;

-(id) initWithDictionary:(NSDictionary*)dic{
    self = [super init];
    if(self){
        id_ = [[dic objectForKey:@"id"] intValue];
        [self setParamsWithDictionary:dic];
    }
    
    return self;
}

-(id) initWithResults:(FMResultSet *)results{
    self = [super init];
    if(self){
        id_ = [results intForColumn:@"id"];
        [self setParams:results];
    }
    
    return self;
}


// abstract
-(void) setParams:(FMResultSet*)results{};
-(void) setParamsWithDictionary:(NSDictionary*)dic{};

@end
