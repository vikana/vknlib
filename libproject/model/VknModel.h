//
//  VknModel.h
//  salon1
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMResultSet.h"

@interface VknModel : NSObject{
    int id_;
}

@property int id_;

-(id) initWithResults:(FMResultSet*)results;
-(id) initWithDictionary:(NSDictionary*)dic;

-(void) setParams:(FMResultSet*)results;
-(void) setParamsWithDictionary:(NSDictionary*)dic;

@end
