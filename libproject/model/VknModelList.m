//
//  VknModelList.m
//  salon1
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import "VknModelList.h"

@implementation VknModelList

-(id) initWithResults:(FMResultSet*)results{
    self = [self init];
    if(self){
        while([results next]){
            [self add:[self createModel:results]];
        }
    }
    
    return self;
}

// abstract
-(VknModel*) createModel:(FMResultSet*)results{
    return nil;
}

-(id)init
{
    self = [super init];
    if(self){
        items = [[NSMutableArray alloc] init];
    }
    
    return self;
}

-(void) clear
{
    [items removeAllObjects];
}

-(void) add:(VknModel*)item{
    [items addObject:item];
}

-(void) add:(VknModel*)item at:(int)at{
    [items insertObject:item atIndex:at];
}

-(void) replace:(VknModel*)item at:(int)at{
    if([items count] <= at){
        [self add:item];
    }else{
        [items replaceObjectAtIndex:at withObject:item];
    }
}

-(int) count
{
    return [items count];
}

-(VknModel*) get:(int)index
{
    return [items objectAtIndex:index];
}

-(VknModel*) getById:(int)id_
{
    int i,max = [self count];
    for(i=0;i<max;i++){
        if([self get:i].id_ == id_)
            return [self get:i];
    }
    
    return nil;
}

-(void) deleteItem:(int)id_{
    VknModel* item = [self getById:id_];
    [items removeObject:item];
}

-(void) removeObjectsInRange:(NSRange)range{
    [items removeObjectsInRange:range];
}

-(void) removeToTail:(int)begin{
    int num = [self count] - begin;
    if(num <= 0)
        return;
    
    [self removeObjectsInRange:NSMakeRange(begin, num)];
}

-(void) reload:(FMResultSet*)results{
    int index = 0;
    while([results next]){
        [self replace:[self createModel:results] at:index];
        index++;
    }
    
    [self removeToTail:index];
}

-(VknModelList*) listOfRange:(NSRange)range{
    VknModelList* modelList = [[VknModelList alloc] init];
    for(int i=range.location,max=range.location+range.length;i<max;i++){
        if(i>=[self count]) break;
        VknModel* model = [self get:i];
        [modelList add:model];
    }
    
    return modelList;
}

//-(void) shuffle{
//    NSArray* tmp = [[NSArray alloc] initWithArray:items];
//    items = [[tmp shuffled] mutableCopy];
//}


@end
