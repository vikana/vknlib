//
//  VknModelList.h
//  salon1
//
//  Created by yu kawase on 2013/12/27.
//  Copyright (c) 2013年 vikana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VknModel.h"

@interface VknModelList : NSObject{
    NSMutableArray* items;
}

-(id) initWithResults:(FMResultSet*)results;
-(void) clear;
-(void) add:(VknModel*)item;
-(void) add:(VknModel*)item at:(int)at;
-(void) replace:(VknModel*)item at:(int)at;
-(int) count;
-(VknModel*) get:(int)index;
-(VknModel*) getById:(int)id_;
-(void) deleteItem:(int)id_;
-(void) removeObjectsInRange:(NSRange)range;
-(void) removeToTail:(int)begin;
-(void) reload:(FMResultSet*)results;
//-(void) shuffle;

-(VknModelList*) listOfRange:(NSRange)range;

@end
